package sample;

import jp.co.dgic.testing.framework.DJUnitTestCase;

public class ProjectBTest extends DJUnitTestCase {

	public ProjectBTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testNotUseVMO() throws Exception {
		ProjectB projectB = new ProjectB();
		String expected = "foo";
		String param = "foo";
		projectB.setProjectB(param);
		String actual = projectB.getProjectB();
		assertEquals(expected, actual);
	}

	public void testUseVMO() throws Exception {
		addReturnValue(ProjectB.class, "getProjectB", "foo");
		ProjectB projectB = new ProjectB();
		String expected = "foo";
		String actual = projectB.getProjectB();
		assertEquals(expected, actual);
	}

	public void testAnotherProjectNotUseVMO() throws Exception{
		ProjectA projectA = new ProjectA();
		String expected = "foo";
		String param = "foo";
		projectA.setProjectA(param);
		String actual = projectA.getProjectA();
		assertEquals(expected, actual);
	}

	public void testAnotherProjectUseVMO() throws Exception{
		addReturnValue(ProjectA.class, "getProjectA", "foo");
		ProjectA projectA = new ProjectA();
		String expected = "foo";
		String actual = projectA.getProjectA();
		assertEquals(expected, actual);
	}

	public void testAnotherTestClassUse() throws Exception {
		ProjectATestUtils.initTest();
	}
}
