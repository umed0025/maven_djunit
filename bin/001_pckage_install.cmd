@echo off
rem install command.
rem mvn install:install-file -Dfile=<path-to-file> -Dsource=<path-to-file> -DgroupId=<group-id> \
rem     -DartifactId=<artifact-id> -Dversion=<version> -Dpackaging=<packaging>

rem LIB_PATHの下に「djunit.jar」「djunit-src.zip」「jcoverage-djunit-1.0.5.jar」「jcoverage-djunit-1.0.5-src.zip」を置いておく。

call %~dp0set_env.cmd

SET LIB_PATH=%~dp0lib

SET LIB_FILE=%LIB_PATH%\djunit.jar
SET LIB_SOURCES=%LIB_PATH%\djunit-src.zip
SET LIB_GROUP_ID=jp.co.dgic.eclipse.jdt.djunit
SET LIB_ARTIFACT_ID=djunit
SET LIB_VERSION=0.8.6
SET LIB_PACKAGEING=jar

call mvn install:install-file -Dfile="%LIB_FILE%" -Dsources="%LIB_SOURCES%" -DgroupId=%LIB_GROUP_ID% ^
 -DartifactId=%LIB_ARTIFACT_ID% -Dversion=%LIB_VERSION% -Dpackaging=%LIB_PACKAGEING% -Dmaven.repo.local=%LOCALREPO%

SET LIB_FILE=%LIB_PATH%\jcoverage-djunit-1.0.5.jar
SET LIB_SOURCES=%LIB_PATH%\jcoverage-djunit-1.0.5-src.zip
SET LIB_GROUP_ID=jp.co.dgic.eclipse.jdt.djunit
SET LIB_ARTIFACT_ID=jcoverage-djunit
SET LIB_VERSION=1.0.5
SET LIB_PACKAGEING=jar

call mvn install:install-file -Dfile="%LIB_FILE%" -Dsources="%LIB_SOURCES%" -DgroupId=%LIB_GROUP_ID% ^
 -DartifactId=%LIB_ARTIFACT_ID% -Dversion=%LIB_VERSION% -Dpackaging=%LIB_PACKAGEING% -Dmaven.repo.local=%LOCALREPO%

pause
