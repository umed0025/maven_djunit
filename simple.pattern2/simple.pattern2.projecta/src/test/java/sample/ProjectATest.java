package sample;

import jp.co.dgic.testing.framework.DJUnitTestCase;

public class ProjectATest extends DJUnitTestCase {

	public ProjectATest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testNotUseVMO() throws Exception {
		ProjectA projectA = new ProjectA();
		String expected = "foo";
		String param = "foo";
		projectA.setProjectA(param);
		String actual = projectA.getProjectA();
		assertEquals(expected, actual);
	}

	public void testUseVMO() throws Exception {
		addReturnValue(ProjectA.class, "getProjectA", "foo");
		ProjectA projectA = new ProjectA();
		String expected = "foo";
		String actual = projectA.getProjectA();
		assertEquals(expected, actual);
	}

}
